# Task manager API v1.0 #

This is a basic documentation for task manager api.

## Getting started ##
### To development and debug ###
* Clone this project
* On the root directory, run `npm install` on terminal
* Run `npm start` and see the magic happen!

The project will run in `http://localhost:3000`.

### To consume ###
`http://task-manager-puc.herokuapp.com/`.

## Routes ##

### (GET) /project/all ###
Retrieves all projects with its tasks

### (GET) /project/:id ###
Retrieve a single project with its tasks

+ Parameters
    + id (int) - ID of project

### (POST) /project/add ###
Add a new project

**Request body**

```
#!json

{
    "name": "Name of project",
    "description": "Description of project"
}
```

### (DELETE) /project/:id ###
Delete a single project

+ Parameters
    + id (int) - ID of project

### (UPDATE) /project/update ###
Update an existing project.

**Request body**

```
#!json
{
    "id": projectId,
    "project": {
        "name": "Name of project",
        "description": "Description of project"
    }
}
```

### (GET) /task/all ###
Retrieve all tasks with it project (_creator).

### (GET) /task/:id ###
Retrieve a single task with its project

+ Parameters
    + id (int) - ID of project

### (POST) /task/search ###
Retrieve tasks matched with search params. Needs at least one of those params bellow.

**Request body**
```
#!json
{
    "search": {
        "name": String,
        "description": String,
        "priority": String,
        "status": String,
        "deadline": Date,
        "_creator": String
    }
}
```

### (POST) /task/add ###
Add a new task

**Request body**
```
#!json
{
	"name": "Task name",
	"description": "Task description",
	"priority": "medium",
	"status": "waiting",
	"deadline": "2016-11-03T03:19:48.286Z", //Javascript Date
	"_creator": "581a9c734415c50f33c16db2" //Project ID
}
```

### (DELETE) /task/:id ###
Delete a single task

+ Parameters
    + id (int) - ID of task

### (POST) /task/update ###
Update an existing task

**Request body**
```
#!json
{
    "id": "taskId", //ID of updated task (required)
    "task": { //New values here (at least one is required)
    	"name": "Task name",
    	"description": "Task description",
    	"priority": "medium",
    	"status": "waiting",
    	"deadline": "2016-11-03T03:19:48.286Z", //Javascript Date
    	"_creator": "581a9c734415c50f33c16db2" //Project ID
	}
}
```
