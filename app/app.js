var express = require('express');
var project = require('./routes/project');
var task = require('./routes/task');
var bodyParser = require('body-parser');
var cors = require('cors');

//Applications routes
var app = express();

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};

app.use(allowCrossDomain);

// app.use(cors());
// app.options('*', cors());

// app.all('/*', function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

//Configs
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


//Hello world!
app.get('/', function(req, res){
  res.send({
    message: '"Não existe trabalho ruim, o ruim é ter que trabalhar" (MADRUGA, Seu).'
  });
});

//Project
app.use('/project', project);

//Task
app.use('/task', task);

//Run app on specified PORT
var PORT = process.env.PORT || 3000;
app.listen(PORT, function(){
  console.log('Listening on port ' + PORT);
});
