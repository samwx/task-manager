var express = require('express');
var Project = require('../models/project');
var api = express.Router();
var connection = require('../connection');

//Get all projects
api.get('/all', function(req, res){
  Project
    .find()
    .populate('tasks')
    .exec(function(err, projects){
      res.send(projects);
    });
});

//Get single project
api.get('/:id', function(req, res){
  Project
    .findById(req.params.id)
    .populate('tasks')
    .exec(function(err, project){
      if (err) res.send({ error: err });

      res.send(project);
    });
});

//Add single project
api.post('/add', function(req, res){
  //Project obj
  var project = new Project({
    name: req.body.name,
    description: req.body.description
  });

  //Save project at mongo DB
  project.save(function (err, project) {
    if(err) return console.error(err);

    res.send({
      message: 'ok',
      project: project
    });
  });
});

//Delete project
api.delete('/:id', function(req, res){
  Project.remove({ _id: req.params.id }, function(err){
    if(err) res.send({ error: err });

    res.send({
      message: 'Removed!'
    });
  });
});

/*
*
* @post: Must receive one object with two keys, as shown: { id: projectId, project: new_project_obj }
* See project model for send new_project key correctly.
*
*/
api.post('/update', function(req, res){
  var query = req.body.id;
  var project = req.body.project;

  Project.findOneAndUpdate(query, project, {new: true}, function(err, doc){
    if(err) return console.log(err);
    res.send(doc);
  });
});

module.exports = api;
