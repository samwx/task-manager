var express = require('express');
var Project = require('../models/project');
var Task = require('../models/task');
var api = express.Router();
var connection = require('../connection');

//Get all tasks
api.get('/all', function(req, res){
  Task.find(function(err, tasks){
    res.send(tasks);
  });
});

//Get single task
api.get('/:id', function(req, res){
  Task.findById(req.params.id, function(err, task){
    if (err) res.send({ error: err });

    res.send(task);
  });
});

//@Post advanced search
api.post('/search', function(req, res){
  Task.find(req.body.search, function(err, task){
    if (err) res.send({ error: err });

    res.send(task);
  });
});

//Add single task
api.post('/add', function(req, res){
  //Assigned task
  var project = Project.findById(req.body._creator, function(err, project){
    //Task obj
    var task = new Task({
      name: req.body.name,
      description: req.body.description,
      priority: req.body.priority,
      status: req.body.status,
      deadline: req.body.date,
      _creator: project._id
    });

    //Save task at mongo DB
    task.save(function (err, task) {
      if(err) return console.error(err);

      res.send({
        message: 'ok',
        task: task
      });
    });

    project.tasks.push(task);
    project.save();
  });
});

//Delete task
api.delete('/:id', function(req, res){
  Task.remove({ _id: req.params.id }, function(err){
    if(err) res.send({ error: err });

    res.send({
      message: 'Removed!'
    });
  });
});

/*
*
* @post: Must receive one object with two keys, as shown: { id: taskId, task: new_task_obj }
* See task model for send task key correctly.
*
*/
api.post('/update', function(req, res){
  var query = req.body.id;
  var task = req.body.task;

  Task.findOneAndUpdate(query, task, {new: true}, function(err, doc){
    if(err) return console.log(err);
    res.send(doc);
  });
});

module.exports = api;
