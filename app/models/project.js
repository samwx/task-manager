var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//Project Schema is like a table, in sql
var projectSchema = Schema({
  name: String,
  description: String,
  tasks : [{ type: Schema.Types.ObjectId, ref: 'Task' }]
});

var projectModel = mongoose.model('Project', projectSchema);

module.exports = projectModel;
