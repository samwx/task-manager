var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//Task Schema is like a table, in sql
var taskSchema = Schema({
  name: String,
  description: String,
  priority: String,
  status: String,
  deadline: { type: Date, default: Date.now },
  _creator: { type: String, ref: 'Project' }
});

var taskModel = mongoose.model('Task', taskSchema);

module.exports = taskModel;
